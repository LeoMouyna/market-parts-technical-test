# Marketparts technical test 🛠

![Marketparts logo](https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-0/p370x247/72126597_100424688049118_9089194818986311680_n.png?_nc_cat=111&_nc_sid=85a577&_nc_ohc=9gzzDfN8nxYAX9q0Wtz&_nc_ht=scontent-cdg2-1.xx&oh=57134ccb674211e25a00d410242b9108&oe=5F83FB2E)

## :office: Architecture

Use nodejs as backend with postgresql as database. Express is used as backend framework over nodejs.

Use Angular as front end.

## :construction_worker: Instructions

The aim of this application is to manage muscic album library.

- Create a music album with at least a cover, a title, a description and a release date.
- Modify an existing music album.
- Delete an existing album
- List music albums by 10 on the default page with a pagination. We can see it title and cover.
- Access to music album detail when clicking on one of the listing ones.

## :cd: Setup

### Prerequisite

You need the following programs:

- [Postgresql](https://www.postgresql.org/download/)
- [Node & npm](https://nodejs.org/en/download/)

### Local start

You first need to `npm i` to install all the needed packages.

You will need to create a user and a database in your postgre instance. Then you have to update `.env` file with your values.

Before starting API server you have to source your .env file `source .env & node server.js`.

API server is running :muscle: !

Then you can start the front-end :star2: with `ng serve`.
