import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { AlbumI } from "../interfaces/album.interface";

const baseUrl = "http://localhost:3000/api/albums";
@Injectable({
  providedIn: "root",
})
export class AlbumService {
  constructor(private http: HttpClient) {}
  size = 5;
  page = 0;

  getAll(page?: number, size?: number): Observable<Object> {
    let qSize = size ? size : this.size;
    let qPage = page ? page : this.page;
    return this.http.get(`${baseUrl}?size=${qSize}&page=${qPage}`);
  }

  get(id: number): Observable<Object> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: AlbumI): Observable<Object> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: AlbumI): Observable<Object> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<Object> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<Object> {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: String, page?: number, size?: number): Observable<Object> {
    let qSize = size ? size : this.size;
    let qPage = page ? page : this.page;
    return this.http.get(
      `${baseUrl}?title=${title}&size=${qSize}&page=${qPage}`
    );
  }
}
