import { Component, OnInit } from "@angular/core";
import { AlbumI, PaginatedAlbumI } from "web/app/interfaces/album.interface";
import { AlbumService } from "web/app/services/album.service";

@Component({
  selector: "app-album-list",
  templateUrl: "./album-list.component.html",
  styleUrls: ["./album-list.component.scss"],
})
export class AlbumListComponent implements OnInit {
  albums: AlbumI[];
  currentAlbum?: AlbumI = null;
  currentIndex = -1;
  title = "";
  search = false;

  page = 1;
  count = 0;
  pageSize = 5;
  pageSizes = [5, 10, 20];

  constructor(private albumService: AlbumService) {}

  ngOnInit(): void {
    this.retrieveAlbums();
  }

  retrieveAlbums(): void {
    const page = this.page - 1;
    this.albumService.getAll(page, this.pageSize).subscribe(
      (data: PaginatedAlbumI) => {
        this.albums = data.albums;
        this.count = data.totalItems;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  setActiveAlbum(album: AlbumI, index: number): void {
    this.currentAlbum = album;
    this.currentIndex = index;
  }

  removeAllAlbums(): void {
    this.albumService.deleteAll().subscribe(
      (_) => {
        this.retrieveAlbums();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteAlbum(albumId: number): void {
    this.albumService.delete(albumId).subscribe(
      (_) => {
        this.currentAlbum = null;
        this.retrieveAlbums();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  searchTitle(): void {
    const page = this.page - 1;
    this.search = true;
    this.albumService.findByTitle(this.title, page, this.pageSize).subscribe(
      (data: PaginatedAlbumI) => {
        this.count = data.totalItems;
        this.albums = data.albums;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  handlePageChange(event): void {
    this.page = event;
    this.retrieveAlbums();
  }

  handlePageSizeChange(event): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.retrieveAlbums();
  }
}
