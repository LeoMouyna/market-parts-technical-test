import { Component, OnInit } from "@angular/core";
import { AlbumService } from "web/app/services/album.service";
import { Router } from "@angular/router";
import { AlbumI } from "web/app/interfaces/album.interface";

@Component({
  selector: "app-add-album",
  templateUrl: "./add-album.component.html",
  styleUrls: ["./add-album.component.scss"],
})
export class AddAlbumComponent implements OnInit {
  album = {
    id: undefined,
    title: "",
    coverUrl: "",
    description: "",
    author: undefined,
    releaseDate: new Date(),
  };

  constructor(private albumService: AlbumService, private router: Router) {}

  ngOnInit(): void {}

  saveAlbum(): void {
    const data = {
      ...this.album,
    };

    this.albumService.create(data).subscribe(
      (response: AlbumI) => {
        const id = response.id;
        this.router.navigate(["/albums", id]);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
