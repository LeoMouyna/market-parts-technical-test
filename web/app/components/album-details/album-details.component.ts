import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlbumI } from "web/app/interfaces/album.interface";
import { AlbumService } from "web/app/services/album.service";

@Component({
  selector: "app-album-details",
  templateUrl: "./album-details.component.html",
  styleUrls: ["./album-details.component.scss"],
})
export class AlbumDetailsComponent implements OnInit {
  currentAlbum?: AlbumI = null;
  message = "";

  constructor(
    private albumService: AlbumService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAlbum(Number(this.route.snapshot.paramMap.get("id")));
  }

  getAlbum(id: number): void {
    this.albumService.get(id).subscribe(
      (data: AlbumI) => {
        this.currentAlbum = data;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateAlbum(): void {
    this.albumService.update(this.currentAlbum.id, this.currentAlbum).subscribe(
      (_) => {
        this.message = "The album was updated successfully!";
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteAlbum(): void {
    this.albumService.delete(this.currentAlbum.id).subscribe(
      (_) => {
        this.router.navigate(["/albums"]);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
