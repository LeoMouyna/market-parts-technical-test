export interface AlbumI {
  id?: number;
  coverUrl: String;
  author?: String;
  description: String;
  releaseDate: Date;
}

export interface PaginatedAlbumI {
  totalItems: number;
  albums: AlbumI[];
  totalPages: number;
  currentPage: number;
}
