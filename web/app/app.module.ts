import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NgxPaginationModule } from "ngx-pagination";
import { AddAlbumComponent } from "./components/add-album/add-album.component";
import { AlbumDetailsComponent } from "./components/album-details/album-details.component";
import { AlbumListComponent } from "./components/album-list/album-list.component";

@NgModule({
  declarations: [
    AppComponent,
    AddAlbumComponent,
    AlbumDetailsComponent,
    AlbumListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
