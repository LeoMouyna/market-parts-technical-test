import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AddAlbumComponent } from "./components/add-album/add-album.component";
import { AlbumDetailsComponent } from "./components/album-details/album-details.component";
import { AlbumListComponent } from "./components/album-list/album-list.component";

const routes: Routes = [
  { path: "", redirectTo: "albums", pathMatch: "full" },
  { path: "albums", component: AlbumListComponent },
  { path: "albums/add", component: AddAlbumComponent },
  { path: "albums/:id", component: AlbumDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
