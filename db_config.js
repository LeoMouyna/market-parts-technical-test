const user = process.env.DB_USER;
const pwd = process.env.DB_PWD;
const host = process.env.DB_HOST;
const database = process.env.DB_DATABASE;

module.exports = {
  HOST: host,
  USER: user,
  PASSWORD: pwd,
  DB: database,
  dialect: "postgres",
};
