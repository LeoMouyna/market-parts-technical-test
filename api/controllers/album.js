const db = require("../models");
const Album = db.albums;
const Op = db.Sequelize.Op;

const getPagingData = (data, page, size) => {
  const { count: totalItems, rows: albums } = data;
  const currentPage = page ? Number(page) : 0;
  const totalPages = Math.ceil(totalItems / size);

  return { totalItems, albums, totalPages, currentPage };
};

const getPagination = (page, size) => {
  const limit = size;
  const offset = page * limit;

  return { limit, offset };
};

// Create and Save a new Album
exports.create = (req, res) => {
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  const album = {
    title: req.body.title,
    author: req.body.author,
    coverUrl: req.body.coverUrl,
    description: req.body.description,
    releaseDate: req.body.releaseDate,
  };

  Album.create(album)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating your Album.",
      });
    });
};

// Retrieve all Albums from the database.
exports.findAll = (req, res) => {
  const { title, page, size } = req.query;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  const { limit, offset } = getPagination(page, size);

  return Album.findAndCountAll({ limit, offset, where: condition })
    .then((data) => {
      res.send(getPagingData(data, page, size));
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving albums.",
      });
    });
};

// Find a single Album with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Album.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((_) => {
      res.status(500).send({
        message: `Error while retrieving Album with id: ${id}`,
      });
    });
};

// Update an Album by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Album.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Album was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Album with id=${id}. Maybe Ambum was not found or req.body is empty!`,
        });
      }
    })
    .catch((_) => {
      res.status(500).send({
        message: `Error while updating Album with id: ${id}`,
      });
    });
};

// Delete an Album with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Album.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Album was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Album with id=${id}. Maybe Album was not found!`,
        });
      }
    })
    .catch((_) => {
      res.status(500).send({
        message: `Error while deleting Album with id: ${id}`,
      });
    });
};

// Delete all Albums from the database.
exports.deleteAll = (req, res) => {
  Album.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Album(s) were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all albums.",
      });
    });
};
